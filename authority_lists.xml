<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.stoa.org/epidoc/schema/9.1/tei-epidoc.rng" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <!-- example: mpese people.xml (this list is in every document) -->
                <title>Authority Lists for the Grammateus project</title>
                <respStmt xml:id="PS">
                    <name>
                        <forename>Paul</forename>
                        <surname>Schubert</surname>
                    </name>
                    <resp>Principal Investigator</resp>
                    <note>
                        <ref type="contact"
                            target="https://www.unige.ch/lettres/antic/unites/grec/enseignants/schubert"
                        />
                    </note>
                </respStmt>
                <respStmt xml:id="SF">
                    <name>
                        <forename>Susan</forename>
                        <surname>Fogarty</surname>
                    </name>
                    <resp>Papyrologist</resp>
                    <note>
                        <ref type="contact"
                            target="https://www.unige.ch/lettres/antic/unites/grec/enseignants/susan-fogarty/"
                        />
                    </note>
                </respStmt>
                <respStmt xml:id="EN">
                    <name>
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                    <resp>Digital Humanities specialist</resp>
                    <note>
                        <ref type="contact"
                            target="https://www.unige.ch/lettres/antic/unites/grec/enseignants/elisa-nury/"
                        />
                    </note>
                </respStmt>
                <respStmt xml:id="LV">
                    <name>
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                    <resp>PhD Student</resp>
                    <note>
                        <ref type="contact"
                            target="https://www.unige.ch/lettres/antic/unites/grec/enseignants/lavinia-ferretti/"
                        />
                    </note>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/"
                        >Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <ab/>
            </sourceDesc>
        </fileDesc>
    </teiHeader>
    <text>
        <body>
            <div>
                <list type="support">
                    <!-- SF: if the fibre is vertical for one side, by default it will horizontal on the other side.
                             this is independent of writing direction (with/against fiber).
                             recto/verso not always a valid distinction for some papyri.
                    -->
                    <item xml:id="fv">Fibre direction: vertical</item>
                    <item xml:id="fh">Fibre direction: horizontal</item>
                    <item xml:id="mixed">Fibre direction: mixed vertical and horizontal
                        fibres</item>
                </list>

                <list type="layout">
                    <head>Description of Text Sections</head>
                    <desc> Each document can be broken into a number of sections and these are
                        highlighted on the display page of every selected papyrus.</desc>
                    <item xml:id="abstract">
                        <label>Abstract</label> An abstract is a summary of the contents of the main
                        text; this is most often found in the sales documents from Pathyris e.g.
                        [242, 125].</item>
                    <item xml:id="heading">
                        <label>Heading</label> A heading can consist of : an official attribution
                        e.g. κόλ(λημα) [18539] (Synchoresis), or παρε (τέθη) [23483] (epikrisis); a
                        category title e.g. a place-name Δρ̣ό̣μ̣(ου) Θ̣[ο]ή̣ρ̣ι̣δ̣(ος) [78616]
                        (epikrisis); a statement of the source of the contents e.g. ἐκ τόμου
                        ἐπικρίσεων [14014, 11379, 14017] (epikriseis).</item>
                    <item xml:id="descr-contents">
                        <label>Description of Content</label> This is a brief decription of what
                        follows e.g. Ζήνωνος ῥίσκος ἐν ὧι ἔνεστι [746](list), γυναικῶν τῶν τὰ ἐρεα
                        ἐργαζομένων [939] (list), or of a payment, ἀπὸ τῆς Παλαμήδους τοῦ Ὀννώφρεως
                        τραπέζης Διονυσιάδος [9115, 9825] (bank receipt).</item>
                    <item xml:id="col-no">
                        <label>Column Number</label> In long documents such as lists and registers
                        the columns may be numbered; documents which are copies from a register, or
                        are part of a τόμος συγκολλήσιμος may also have a column number e.g. [15744,
                        11410].</item>
                    <item xml:id="official-mark">
                        <label>Official Mark</label> A mark on the papyrus probably made by an
                        official see e.g. the large X at the start of these epikrisis declarations
                        [20324, 21853].</item>
                    <item xml:id="addressee">
                        <label>Addressee</label> Highlighting the unusual position of addressee in some business notes.
                    </item>
                    <item xml:id="introduction">
                        <label>Opening</label> This consists of various formulaic constructions with
                        the name of the addressor and recipient, and other pertinent information; it
                        can be followed by an opening salutation e.g. χαίρειν.</item>
                    <item xml:id="main-text">
                        <label>Main text</label> This contains the statement, declaration, request,
                        or the details of a transaction, the people involved, and includes the main
                        verb.</item>
                    <item xml:id="subscription">
                        <label>Closing</label> This comes after the main text and can consist of one
                        or more of the following elements: salutation: a closing salutation, if
                        present, usually comes before any other part of the subscription (ἐυτύχει,
                        ἔρρωσο); signature: σεσημείωμαι; transmission docket: ἐπιδέδωκα; apostil: an
                        official note relevant to the contents of the document, usually written in a
                        hand other than that of the main text e.g. the apostil at the end of the
                        petition [3676].</item>
                    <item xml:id="salutation">
                        <label>Salutation</label> When the closing salutation is relevant to the
                        typology, it is highlighted in the text (e.g. Business letters, TM 691, 677
                        etc.)</item>
                    <item xml:id="date">
                        <label>Date</label> The date the document was written can be positioned
                        before the introduction, between the introduction and main text, or anywhere
                        in the subscription; dates within the main text are not highlighted.</item>
                    <item xml:id="trans-clause">
                        <label>Transitional clause</label> this is usually found in lists and
                        registers and can mark the division between a description of the contents
                        and the main text e.g. ἔστι δέ [12684, 8760], or εἶναι δέ [21632].</item>
                    <item xml:id="signature">
                        <label>Signature</label> Indicates the presence of the scribe Hermas'
                        signature, in the Libelli. It can also occasionally highlight another
                        signature [13481].</item>
                </list>
            </div>
        </body>
    </text>
</TEI>
